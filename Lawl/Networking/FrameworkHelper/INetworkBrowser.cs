using System.Collections.Generic;

namespace Lawl.Networking.FrameworkHelper
{
    public interface INetworkBrowser
    {
        List<string> GetNetworkComputers();
    }
}