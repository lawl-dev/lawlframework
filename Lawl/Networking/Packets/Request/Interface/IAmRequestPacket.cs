namespace Lawl.Networking.Packets.Request.Interface
{
    public interface IAmRequestPacket
    {
        string PacketGuid { get; set; }
    }
}