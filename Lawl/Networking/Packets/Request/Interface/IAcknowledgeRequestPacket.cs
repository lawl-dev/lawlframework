namespace Lawl.Networking.Packets.Request.Interface
{
    public interface IAcknowledgeRequestPacket
    {
        string ProtocolName { get; }
    }
}