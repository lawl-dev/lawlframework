namespace Lawl.Networking.Packets.Answer.Interface
{
    public interface IAcknowledgedPacket
    {
        string ProtocolName { get; }
    }
}