namespace Lawl.Networking.Packets.Answer.Interface
{
    public interface IAmAnswer
    {
        string RequestPacketGuid { get; set; }
    }
}