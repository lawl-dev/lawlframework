﻿namespace Lawl.Networking.Packets.ConstantClass
{
    internal static class ValueTypes
    {
        public const string Int32 = "Int32";
        public const string String = "String";
        public const string GenericList = "List`1";
        public const string Float = "Single";
        public const string Bool = "Boolean";
    }
}
